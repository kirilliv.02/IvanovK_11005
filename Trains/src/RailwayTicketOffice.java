import java.util.Scanner;

public class RailwayTicketOffice {

    Scanner sc = new Scanner(System.in);

    static Passenger passenger = new Passenger();

    public static final String RESET = "\u001B[0m";
    public static final String RED = "\u001B[31m";
    public static final String GREEN = "\u001B[32m";


    public static void main(String[] args) {
        Trains trains = new Trains();
        trains.setTrains(new Trains(55, new String[]{"A", "B", "C"}, 200));
        trains.setTrains(new Trains(12, new String[]{"D", "A", "E"}, 300));
        trains.setTrains(new Trains(16, new String[]{"E", "G", "A"}, 400));
        RailwayTicketOffice railwayTicketOffice = new RailwayTicketOffice();
        railwayTicketOffice.menu();
    }


    public void menu() {
        System.out.println("[1] Пассажир");
        System.out.println("[2] Админ");
        System.out.println("[3] Выход");
        char key = sc.next().charAt(0);
        switch (key) {
            case '1' -> {
                boolean exit = false;
                while (!exit) {
                    System.out.println("[1] Создать заявку");
                    System.out.println("[2] Назад");
                    char key1 = sc.next().charAt(0);
                    switch (key1) {
                        case '1' -> passenger.addRequest();
                        case '2' -> exit = true;
                        default -> errorString("-----\nError! Please choose again:\n-----");
                    }
                }
                menu();
            }
            case '2' -> new Administrator().startAdmin();
            case '3' -> successString("Пока :)");
            default -> {
                errorString("-----\nError! Please choose again:\n-----");
                menu();
            }
        }
    }
    public void errorString(String string){
        System.out.println(RED + string + RESET);
    }

    public void successString(String string){
        System.out.println(GREEN + string + RESET);
    }


}
