import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Passenger {

    private String name;
    private String date;
    private String time;
    private String station;
    private int train;
    private int cost;

    public Passenger() {
    }

    public Passenger(String name, String date, String time, String station, int train, int cost) {
        this.name = name;
        this.date = date;
        this.time = time;
        this.station = station;
        this.train = train;
        this.cost = cost;
    }

    private static final ArrayList<Passenger> passengers = new ArrayList<>();
    Trains trains = new Trains();

    private final Scanner sc = new Scanner(System.in);

    static RailwayTicketOffice railwayTicketOffice = new RailwayTicketOffice();

    public void addRequest() {
        String name = addName();
        String date = addDate();
        String time = addTime();
        String station = addStation();
        Trains train = addTrain(station);

        Passenger passenger = new Passenger(name, date, time, station, train.getNumberTrain(), train.getCost());
        RailwayTicketOffice railwayTicketOffice = new RailwayTicketOffice();
        addPassenger(passenger);

        railwayTicketOffice.successString("Заявка успешно оставлена!");
        railwayTicketOffice.menu();
    }


    private String addName() {
        Pattern pattern = Pattern.compile("[A-ZaА-Я][a-zа-я]{2,10}");
        String name;
        while (true) {
            System.out.print("Введите имя с заглавной буквы: ");
            name = sc.next();
            if (!pattern.matcher(name).matches()) {
                railwayTicketOffice.errorString("Имя введено неверно, попробуйте еще раз!");
            } else break;
        }
        return name;
    }

    private String addDate() {
        Pattern pattern = Pattern.compile("[0-9]{2}[.][0-9]{2}[.][0-9]{2}");
        String date;
        while (true) {
            System.out.print("Введите дату в формате дд.мм.гг: ");
            date = sc.next();
            if (!pattern.matcher(date).matches()) {
                railwayTicketOffice.errorString("Дата введена неверно, попробуйте еще раз!");
            } else break;
        }
        return date;
    }

    private String addTime() {
        Pattern pattern = Pattern.compile("[0-9]{2}[:][0-9]{2}");
        String time;
        while (true) {
            System.out.print("Введите время в формате чч:мм: ");
            time = sc.next();
            if (!pattern.matcher(time).matches()) {
                railwayTicketOffice.errorString("Время введено неверно, попробуйте еще раз!");
            } else break;
        }
        return time;
    }

    private String addStation() {
        String station;
        while (true) {
            System.out.print("Введите станцию: ");
            station = sc.next();
            if (trains.checkStation(station)) {
                break;
            } else {
                railwayTicketOffice.errorString("Данной станции не существует, поробуйте еще раз!");
                trains.allStations();
            }
        }
        return station;
    }

    private Trains addTrain(String station) {
        int countTrains = 0;
        ArrayList<Trains> trainList = new ArrayList<>();
        System.out.println("Выберете номер поезда: ");
        for (int i = 0; i < trains.getTrains().size(); i++) {
            for (int j = 0; j < trains.getTrains().get(i).getStations().length; j++) {
                if (trains.getTrains().get(i).getStations()[j].equals(station)) {
                    countTrains++;
                    trainList.add(trains.getTrains().get(i));
                    break;
                }
            }
        }
        for (int i = 0; i < trainList.size(); i++) {
            System.out.println("[" + (i + 1) + "]" + " Поезд №" + trainList.get(i).getNumberTrain() + ", Цена: " + trainList.get(i).getCost());
        }

        int train;
        while (true) {
            train = sc.nextInt();
            if (train < 1 | train > countTrains) {
                railwayTicketOffice.errorString("Ошибка! Поробуйте еще раз");
            } else break;
        }
        return trainList.get(train - 1);
    }

    public void addPassenger(Passenger passenger) {
        passengers.add(passenger);
    }

    public ArrayList<Passenger> getPassenger() {
        return passengers;
    }

    @Override
    public String toString() {
        return "Имя: " + name
                + "\nДата поездки: " + date
                + "\nВремя поездки: " + time
                + "\nСтанция назначения: " + station
                + "\nПоезд № " + train
                + "\nЦена билета: " + cost
                + "\n-----------";
    }
}
