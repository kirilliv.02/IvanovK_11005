import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Trains {
    private static final ArrayList<Trains> trains = new ArrayList<>();

    private int numberTrain;
    private String[] stations;
    private int cost;

    public Trains() {
    }

    public Trains(int numberTrain, String[] stations, int cost) {
        this.numberTrain = numberTrain;
        this.stations = stations;
        this.cost = cost;
    }

    RailwayTicketOffice railwayTicketOffice = new RailwayTicketOffice();

    Scanner sc = new Scanner(System.in);

    void addTrain() {
        int numberTrain = addNumberTrain();
        int stationQuantity = addStationQuantity();
        String[] stations = new String[stationQuantity];
        for (int i = 0; i < stationQuantity; i++) {
            System.out.println("Станция №" + (i + 1) + ":");
            stations[i] = sc.next();
        }
        int cost = addCost();
        Trains train = new Trains(numberTrain, stations, cost);
        trains.add(train);
        railwayTicketOffice.successString("Поезд успешно добавлен!");
    }

    private int addNumberTrain() {
        Pattern pattern = Pattern.compile("[1-9][0-9]{0,2}");
        String numberTrain;
        while (true) {
            System.out.print("Введите номер поезда: ");
            numberTrain = sc.next();
            if (!pattern.matcher(numberTrain).matches()) {
                System.out.println("Номер введен неверно, попробуйте еще раз!");
            } else break;
        }
        return Integer.parseInt(numberTrain);
    }

    private int addStationQuantity() {
        Pattern pattern = Pattern.compile("[1-9][0-9]?");
        String stationQuantity;
        while (true) {
            System.out.print("Введите колтчество станций: ");
            stationQuantity = sc.next();
            if (!pattern.matcher(stationQuantity).matches()) {
                railwayTicketOffice.errorString("Количество станций введено неверно, попробуйте еще раз!");
            }  else {
                if (Integer.parseInt(stationQuantity) > 10 | Integer.parseInt(stationQuantity) < 1){
                    railwayTicketOffice.errorString("Минимальное количество станций - 10, максимальное - 10 ");
                } else break;
            }
        }
        return Integer.parseInt(stationQuantity);
    }

    private int addCost() {
        Pattern pattern = Pattern.compile("[1-9][0-9]{0,3}");
        String cost;
        while (true) {
            System.out.print("Введите цену за билет: ");
            cost = sc.next();
            if (!pattern.matcher(cost).matches()) {
                railwayTicketOffice.errorString("Цена введена неверно, попробуйте еще раз!");
            } else {
                if (Integer.parseInt(cost) > 3000 | Integer.parseInt(cost) < 100) {
                    railwayTicketOffice.errorString("Миниммальная цена  - 100, максимальная - 3000");
                } else break;
            }
        }
        return Integer.parseInt(cost);
    }

    public void checkTrains() {
        if (trains.size() == 0) {
            railwayTicketOffice.errorString("Поезда не добавленны!");
        } else {
            for (Trains train : trains) {
                trainToString(train);
            }
            railwayTicketOffice.successString("Количество поездов: " + trains.size());
        }
    }

    boolean checkStation(String station) {
        boolean check = false;
        for (Trains train : trains) {
            for (int j = 0; j < train.stations.length; j++) {
                if (train.stations[j].equals(station)) {
                    check = true;
                    break;
                }
            }
        }
        return check;
    }

    public void trainToString(Trains train) {
        int numberTrain = train.numberTrain;
        String[] stations = train.stations;
        int cost = train.cost;
        System.out.println("Номер поезда: " + numberTrain);
        System.out.print("Станции: ");
        for (String station : stations) {
            System.out.print(station + " ");
        }
        System.out.println();
        System.out.println("Цена за билет: " + cost);
        System.out.println("-----------------------");
    }

    public void allStations() {

        ArrayList<String> stations = new ArrayList<>();

        for (Trains train : trains) {
            Collections.addAll(stations, train.stations);
        }
        if (stations.size()==0) {
            railwayTicketOffice.errorString("Станции не найдены!");
            RailwayTicketOffice railwayTicketOffice = new RailwayTicketOffice();
            railwayTicketOffice.menu();
        } else {
            System.out.print("Доступные станции: " + stations.get(0) + " ");
            int count = 0;
            for (int i = 1; i < stations.size(); i++) {
                for (int j = 0; j < i; j++) {
                    if (stations.get(i).equals(stations.get(j))) {
                        count++;
                        break;
                    }
                }
                if (count != 0) {
                    count = 0;
                } else {
                    System.out.print(stations.get(i) + " ");
                }

            }
            System.out.println();
        }

    }

    public ArrayList<Trains> getTrains() {
        return trains;
    }

    public void setTrains(Trains train) {
        trains.add(train);
    }


    public int getNumberTrain() {
        return numberTrain;
    }

    public String[] getStations() {
        return stations;
    }

    public int getCost() {
        return cost;
    }


}
