import java.util.Scanner;

public class Administrator {

    private static final int checkPass = 12345;

    RailwayTicketOffice railwayTicketOffice = new RailwayTicketOffice();
    Trains trains = new Trains();
    Passenger passenger = new Passenger();

    Scanner sc = new Scanner(System.in);

    void adminMenu() {
        System.out.println("[1] Посмотреть заявки");
        System.out.println("[2] Добавить поезда и станции");
        System.out.println("[3] Посмотреть поезда и станции");
        System.out.println("[4] Назад");
        char key1 = sc.next().charAt(0);
        switch (key1) {
            case '1' -> {
                checkRequests();
                adminMenu();
            }
            case '2' -> {
                trains.addTrain();
                adminMenu();
            }
            case '3' -> {
                trains.checkTrains();
                adminMenu();
            }
            case '4' -> railwayTicketOffice.menu();
            default -> {
                railwayTicketOffice.errorString("-----\nError! Please choose again:\n-----");
                adminMenu();
            }
        }
    }

    public void startAdmin() {
        System.out.print("Password: ");

        int password = sc.nextInt();
        if (password == checkPass) {
            railwayTicketOffice.successString("Access accepted!");
            adminMenu();
        } else {
            railwayTicketOffice.errorString("Access denied. Try again!");
            startAdmin();
        }
    }

    void checkRequests() {
        if (passenger.getPassenger().size() == 0) {
            railwayTicketOffice.errorString("Заявок нет");
        } else {
            for (int i = 0; i < passenger.getPassenger().size(); i++) {
                System.out.println(passenger.getPassenger().get(i).toString());
            }
            railwayTicketOffice.successString("Количество заявок: " + passenger.getPassenger().size());
        }
    }

}
