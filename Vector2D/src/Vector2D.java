public class Vector2D {

    private double x,y;

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void add(Vector2D other){
        this.x += other.x;
        this.y += other.y;
        printVector("Sum",this.x, this.y);
    }

    public void sub(Vector2D other){
        this.x -= other.x;
        this.y -= other.y;
        printVector("Sub", this.x, this.y);
    }
    public void mult(double t){
        this.x *= t;
        this.y *= t;
        printVector("Mult", this.x, this.y);
    }

    private void printVector(String str, double x, double y){
        System.out.println(str + ":" + " (" + x + "; " + y + ")");
    }

}
