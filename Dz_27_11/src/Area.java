import java.util.Scanner;

public class Area {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Shape[] shapes = new Shape[6];

        System.out.println("Are of first rectangle. Enter length and width: ");
        shapes[0] = new Rectangle( sc.nextDouble(), sc.nextDouble());
        System.out.println("Are of second rectangle. Enter length and width: ");
        shapes[1] = new Rectangle( sc.nextDouble(), sc.nextDouble());
        System.out.println("Are of first circle. Enter a radius: ");
        shapes[2] = new Circle(sc.nextDouble(),3.14);
        System.out.println("Are of second circle. Enter a radius: ");
        shapes[3] = new Circle(sc.nextDouble(),3.14);
        System.out.println("Are of first triangle. Enter height and base: ");
        shapes[4] = new Triangle(sc.nextDouble(), sc.nextDouble());
        System.out.println("Are of second triangle. Enter height and base: ");
        shapes[5] = new Triangle(sc.nextDouble(), sc.nextDouble());

        for (int i = 0; i < shapes.length; i++) {
            double area = shapes[i].getArea();
            System.out.println(area);

        }
    }
}
