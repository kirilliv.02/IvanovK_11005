package com.kirilliv02.store;

import java.util.ArrayList;

public class Customer {
    private String name;
    private int age;
    private String sex;

    public Customer(String name, int age, String sex){
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public Order buy(Product product) {
        return new Order(product, this);
    }

    public void buy(Product product, ArrayList orders) {
        Order order = new Order(product, this);
        orders.add(String.valueOf(order));
    }
}
