package com.kirilliv02.store;

public class Product {
    private String name;
    private int price;
    private String manufacturer;

    public Product(String name, int price, String manufacturer){
        this.name = name;
        this.price = price;
        this.manufacturer = manufacturer;
    }
    @Override
    public String toString() {
        return "Product: " + name + " " + price + " " + manufacturer;
    }
}
