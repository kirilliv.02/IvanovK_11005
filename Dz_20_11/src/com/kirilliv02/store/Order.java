package com.kirilliv02.store;

public class Order {
    private Product product;
    private Customer customer;

    public Order (Product product, Customer customer) {
        this.product = product;
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "new Order: \n" + customer + "\n" + product;
    }
}