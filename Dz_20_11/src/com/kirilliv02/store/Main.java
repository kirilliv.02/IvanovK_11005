package com.kirilliv02.store;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList requests = new ArrayList();
        Customer customer1 = new Customer("Timur", 17, "M");
        Product product1 = new Product("Burn", 87, "Pyaterochka");
        customer1.buy(product1, requests);
        System.out.println(requests);

        Customer customer2 = new Customer("Kirill", 18, "M");
        Product product2 = new Product("milk", 55, "Pyaterochka");
        customer2.buy(product2, requests);
        System.out.println(requests);
    }
}
