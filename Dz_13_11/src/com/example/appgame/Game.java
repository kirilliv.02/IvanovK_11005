package com.example.appgame;

import java.util.Scanner;

public class Game {
    public static void main(String[] args) {

        Player [] player = new Player[2];
        Scanner sc = new Scanner(System.in);

        System.out.println("Игрок 1, введите ваше имя:");
        String firstPlayerName = sc.nextLine();
        System.out.println("Игрок 2, введите ваше имя:");
        String secondPlayerName = sc.nextLine();

        player[0] = new Player(firstPlayerName, 100);
        player[1] = new Player(secondPlayerName, 100);

        while (true) {
            System.out.println(player[0].getName() + " введите силу удара от 1 до 9:");

            player[1].damage();
            System.out.println("У " + player[1].getName() + " " + player[1].getHp() + " хп");
            if (player[1].checkHp()) {
                player[0].gameOver();
                break;
            }
            System.out.println(player[1].getName() + " введите силу удара от 1 до 9:");
            player[0].damage();
            System.out.println("У " + player[0].getName() + " " + player[0].getHp() + " хп");
            if (player[0].checkHp()) {
                player[1].gameOver();
                break;
            }
        }
    }
}