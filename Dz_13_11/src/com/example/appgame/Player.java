package com.example.appgame;

import java.util.Scanner;

public class Player {
    private final String name;
    private int hp;

    public Player(String name, int hp) {
        this.name = name;
        this.hp = hp;
    }

    public boolean checkHp() {
        return this.hp <= 0;
    }

    public void gameOver() {
        System.out.println(this.name + " выйграл");
    }

    public void damage() {
        Scanner sc = new Scanner(System.in);
        int strength = sc.nextInt();
        if (strength < 1 | strength > 9) {
            System.out.println("Ошибка, введите заного:");
            damage();
        } else {
            int random = 1 + (int) (Math.random()*9);
            if (random-strength+1 >= 0) {
                System.out.println("Попал: -" + strength + " хп");
                this.hp -= strength;
            } else System.out.println("Промах: -0 хп");
        }
    }

    public String getName() {
        return this.name;
    }

    public int getHp() {
        return this.hp;
    }
}
